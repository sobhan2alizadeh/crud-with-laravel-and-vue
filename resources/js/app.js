require('./bootstrap');

require('alpinejs');
import {createApp} from "vue";
import router from "./router"
import CompaniesIndex from "./components/componies/CompaniesIndex";
createApp({
    components:{
        CompaniesIndex
    }
}).use(router).mount('#app')
